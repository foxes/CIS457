package lab3;

import java.io.*;
import java.net.*;

//Halston Raddatz
//CIS 457 - Fall 2016
//Lab 3
////////////////////////

public class Server {

	private static ServerSocket serverSocket;

	public static void main(String[] args) throws IOException {

		
	    BufferedReader inFromUser = 
		new BufferedReader(new InputStreamReader(System.in));   
		
        System.out.println("Please enter a port number to listen: ");  
        
	    int port = Integer.parseInt(inFromUser.readLine());

    	System.out.println("Listening for connections "); 

		serverSocket = new ServerSocket(port);
		Socket clientSocket = serverSocket.accept();

    
		rFromClient recieve = new rFromClient(clientSocket);
		Thread thread = new Thread(recieve);
		thread.start();

		sToClient send = new sToClient(clientSocket);
		Thread thread2 = new Thread(send);
		thread2.start();
	}}

class rFromClient implements Runnable
{
	Socket clientSocket=null;
	BufferedReader bufferedReader = null;
	
	public rFromClient(Socket clientSocket)
	{
		this.clientSocket = clientSocket;
	}
	public void run() {
		try{
		bufferedReader = new BufferedReader
		            (new InputStreamReader(this.clientSocket.getInputStream()));		
		
		String messageString;
		while(true){
		while((messageString = bufferedReader.readLine())!= null){
			if(messageString.equals("QUIT"))
			{
				break;
			}
			System.out.println("Client: " + messageString);
			System.out.println("~~Enter message for client: ");
		}
		this.clientSocket.close();
		System.exit(0);
	}
		
	}
	catch(Exception ex){System.out.println(ex.getMessage());}
	}
}

class sToClient implements Runnable
{
	PrintWriter Writer;
	Socket clientSock = null;
	
	public sToClient(Socket clientSock)
	{
		this.clientSock = clientSock;
	}
	public void run() {
		try{
		    
		Writer = new PrintWriter
		(new OutputStreamWriter(this.clientSock.getOutputStream()));
		
		while(true)
		{
			String messageToClient = null;
			BufferedReader input = 
			new BufferedReader(new InputStreamReader(System.in));
			
			messageToClient = input.readLine();
			
			Writer.println(messageToClient);
			Writer.flush();
			System.out.println("~~Enter message for client: ");
		}
		}
		catch(Exception ex){System.out.println(ex.getMessage());}	
	}
}

