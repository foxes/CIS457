package lab3;

import java.io.*;
import java.net.*;

//Halston Raddatz
//CIS 457 - Fall 2016
//Lab 3
////////////////////////

public class Client {
	public static void main(String[] args)
	{
		try {

	
			BufferedReader PortFromUser = 
			new BufferedReader(new InputStreamReader(System.in));    				
			System.out.println("Enter a port: "); 	
			int Port = Integer.parseInt(PortFromUser.readLine()); 

    		BufferedReader IPFromUser = 
    		new BufferedReader(new InputStreamReader(System.in));    				
			System.out.println("Enter an IP: "); 	
			String IP = IPFromUser.readLine();    			
			
			Socket sock = new Socket(IP,Port);

			sThread sendThread = new sThread(sock);
			Thread thread = new Thread(sendThread);
			thread.start();

			rThread recieveThread = new rThread(sock);
			Thread thread2 = new Thread(recieveThread);
			thread2.start();

		} catch (Exception e) {System.out.println(e.getMessage());} 
	}
}
class rThread implements Runnable
{
	Socket socket=null;
	BufferedReader recieve=null;
	
	public rThread(Socket socket) {
		this.socket = socket;
	}
	public void run() {
		try{
		recieve = 
		new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		String ReceivedMessage = null;
		while((ReceivedMessage = recieve.readLine())!= null)
		{
			System.out.println("Server: " + ReceivedMessage);
			System.out.println("~~Enter a message for Server: ");
		}
		}catch(Exception e){System.out.println(e.getMessage());}
	}
}
class sThread implements Runnable
{
	Socket sock=null;
	PrintWriter print=null;
	BufferedReader brinput=null;
	
	public sThread(Socket sock)
	{
		this.sock = sock;
	}
	public void run(){
		try{
		if(sock.isConnected())
		{
			this.print = new PrintWriter(sock.getOutputStream(), true);	
			System.out.println
			("Type your message to send to server..type 'QUIT' to QUIT");
			
		while(true){
		
			brinput = new BufferedReader(new InputStreamReader(System.in));
			String messageToServer=null;
			messageToServer = brinput.readLine();
			this.print.println(messageToServer);
			this.print.flush();
		
			if(messageToServer.equals("QUIT"))
			break;
			}
		
		sock.close();}}catch(Exception e){System.out.println(e.getMessage());}
	}
}
