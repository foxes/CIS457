import java.io.*;
import java.net.*;
import java.util.ArrayList;


//Halston Raddatz
//CIS 457 - Fall 2016
//Lab 4
/////////////////////
class UDPServer {
    public static void main (String[] args) throws Exception {

	BufferedReader inFromUser = 
		new BufferedReader(new InputStreamReader(System.in));
	System.out.println("Please enter a port number to listen: ");
	int port = Integer.parseInt(inFromUser.readLine());

        DatagramSocket serverSocket = new DatagramSocket(port);
        ArrayList<ConnectedClient> clientList = new ArrayList<ConnectedClient>();
        while(true) {
            byte[] ReceivedData = new byte[1024];
            byte[] sendData = new byte[1024];
            DatagramPacket ReceivedPacket =
                new DatagramPacket(ReceivedData, ReceivedData.length);
            serverSocket.receive(ReceivedPacket);
            
            String message = new String(ReceivedPacket.getData());
			message = message.trim();
            
            InetAddress ipAddress = ReceivedPacket.getAddress();
            int port = ReceivedPacket.getPort();
            
            boolean existingClient = false;
            for (ConnectedClient client: clientList) {
                if (client.getIP().equals(ipAddress) &&
                            client.getPort() == port) {
                    existingClient = true;
                    break;
                }
            }
            if (!existingClient) {
                ConnectedClient newClient = new ConnectedClient(ipAddress, port);
                clientList.add(newClient);
                System.out.printf("client connected on: IP: %s Port: %d\n\n",
				   ipAddress.getHostAddress(), port);
            }

            if (message.equals("/exit")) {
                int i=0;
                while (i<clientList.size() &&
                        !clientList.get(i).getIP().equals(ipAddress) &&
                        clientList.get(i).getPort() != port) {
                    i++;
                }
                clientList.remove(i);
                System.out.printf("Client disconnected from IP: %s Port: %d\n",
				  ipAddress.getHostAddress(), port);
            } else {
		System.out.println("Got Message from " + ipAddress.getHostAddress() +
				    ":" + port + ": " + message);
		    }
			
	    message = ipAddress.getHostAddress() + ":" + port + " said: " + message;
            sendData = message.getBytes();

            for (ConnectedClient client : clientList) {
                if (!client.getIP().equals(ipAddress) || 
                            client.getPort() != port) {
                    DatagramPacket sendPacket = 
                     new DatagramPacket(sendData,sendData.length,client.getIP(), 
					  client.getPort());
                    serverSocket.send(sendPacket);
                    System.out.printf("Sent to %s:%d\n",
		                     client.getIP().getHostAddress(),client.getPort());
                }
            }
			System.out.println();
            String successMessage = "(message sent)";
            sendData = successMessage.getBytes();
            DatagramPacket successPacket =
                new DatagramPacket(sendData,sendData.length,ipAddress,port);
                serverSocket.send(successPacket);
        }
    }
}

class ConnectedClient {
    InetAddress ip;
    int port;

    ConnectedClient(InetAddress ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    InetAddress getIP() {
        return this.ip;
    }

    int getPort() {
        return this.port;
    }

}
