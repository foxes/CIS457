/*
Halston Raddatz
Bryana Craig
CIS 457 Fall 2016
Project 1
 */
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client {

    public final static int FILE_SIZE = 602000000;

    public static void main(String[] args) throws IOException {
        int bytes;
        int inProgress = 0;
        Socket sSocket = null;
        BufferedOutputStream bufferedOut = null;

        //Scanning in serverAddress(IP) and port
        String serverAddress, port;
        Scanner uInput = new Scanner(System.in);
        
        System.out.println("Enter an IP address (localhost 127.0.0.1): ");
        serverAddress = uInput.next();
        
        System.out.println("Enter a port (Default 9876): ");
        port = uInput.next();
        
        //uses isValidIP and isValidPort methods to decide if the values are valid
        if (isValidIP(serverAddress) == true && isValidPort(port) == true) {} else {
            System.out.print("Bad IP or port.");
            System.exit(0);
        }


        try {

            while (true) {
                //creating the socket, uses parseInt to convert users
                //Port uInput from a string to an Int
                sSocket = new Socket(serverAddress, Integer.parseInt(port));

                //receiving user uInput, either a filename or exit signal
                BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Please enter a file name or /exit to quit: ");
                String fileName = inFromUser.readLine();

                //if user uInput equals "/exit", should close socket and exit
                if (fileName.equals("/exit")) {
                    System.out.println("exiting client.. ");
                    bufferedOut.close();
                    sSocket.close();
                    System.exit(0);
                }

                //creating the datastream with the socket
                DataOutputStream outToServer =
                    new DataOutputStream(sSocket.getOutputStream());
                
                //sending filename to server CLI
                outToServer.writeBytes(fileName + '\n');

                //receiving file using
                byte[] byteArray = new byte[FILE_SIZE];
                InputStream inputStream = sSocket.getInputStream();
                
                //checking if inputStream is available for use
                if (inputStream.available() > 0) {
                    bufferedOut =
                        new BufferedOutputStream(
                            new FileOutputStream("NEW " + fileName));

                    bytes = inputStream.read(byteArray, 0, byteArray.length);
                    inProgress = bytes;

                    do {
                        //logic for determining if entire file has been transfered
                        bytes =
                        inputStream.read(byteArray, inProgress, (byteArray.length - inProgress));
                      
                        if (bytes >= 0) inProgress += bytes;
                        
                    //as long as bytes is greater than -1, transfer proceeds
                    } while (bytes > -1);

                    bufferedOut.write(byteArray, 0, inProgress);
                    bufferedOut.flush();
                    System.out.println("File " + fileName + " transferred (" + inProgress + " bytes read)");
                } else {
                    System.out.println("File can not be located");
                }
            }
        } catch (Exception e) {
            System.out.println("exiting..");
            
        }
            
           //closes socket when complete
           finally {
            if ( bufferedOut != null) bufferedOut.close();
            if ( sSocket != null) sSocket.close();
        }
    }

    //Checking string for valid IP format
    //param String ip - string to be checked
    //returns True/False
    public static boolean isValidIP(String serverAddress) {
      
            //splitting given strip by periods
            String[] characters = serverAddress.split("\\.");
            
            //valid IP can not be over 4 sections of characters
            if (characters.length > 4) return false;
            int character;
            try {
                //parsing ints from array
                for (int i = 0; i < 4; i++) {
                    try {
                      
                        character = Integer.parseInt(characters[i]);
                        
                    //catches exception if they cant be parsed into ints
                    } catch (NumberFormatException e) {
                      
                        return false;
                    }
                    
                    if (i != 3) {
                      
                        if (character < 0 || character > 255) return false;
                        
                    } else if (character < 1 || character > 254) return false;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
              
                e.printStackTrace(System.out);
            }
            return true;
        }
    //Checking String port for valid Port format
    //Ports must be between 0 and 65535
    //@param String port - port to be checked
    //returns True/false
    public static boolean isValidPort(String port) {
        int uInput;
        try {
            uInput = Integer.parseInt(port);
        } catch (NumberFormatException e) {
            return false;
        }
        if (uInput < 0 || uInput > 65535) return false;
        return true;
    }

}
