/*
Halston Raddatz
Bryana Craig
CIS 457 Fall 2016
Project 1
 */
import java.io.*;
import java.nio.*;
import java.math.*;
import java.net.*;

//Server class for the client to connect to.
public class Server {

    private int numberOfClients;

    public Server() throws IOException {

	    //parses user input (string) into int variable Port
            BufferedReader userGivenPort = 
	    new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter a port to listen on");
            int Port = Integer.parseInt(userGivenPort.readLine());

	    //creating Socket with Port from user
            ServerSocket serverSocket = new ServerSocket(Port);

	    //Keeping track of # of clients connected
            for (numberOfClients = 0; true; numberOfClients++) {
                System.out.println("Active Connections: " + numberOfClients);
                final Socket sSocket = serverSocket.accept();
                new Thread(new Runnable() {@
                    Override
                    public void run() {
                        try {
                            handleConnection(sSocket);
			//catching exceptions
                        } catch (IOException e) {
                            System.out.println("Client disconnected.");
                            numberOfClients--;
                            System.out.println("Number of Connections: " + numberOfClients);
			//if client disconnects first, might trigger NullPointerException
                        } catch (NullPointerException e) {
                            System.out.println("Client disconnected.");
                            numberOfClients--;
                            System.out.println("Number of Connections: " + numberOfClients);
                        }
                    }
                }).start();
            }
        }
    // This method handles the Socket connection from the clients
    public void handleConnection(Socket sSocket) throws IOException {
            BufferedReader input = 
	    new BufferedReader(new InputStreamReader(sSocket.getInputStream()));

            String line;
            line = input.readLine();

            System.out.println(line);

            if (line.equals("/exit")) {
                System.out.println("Client Disconnected\nClosing Connection");
                input.close();
                sSocket.close();             
                numberOfClients--;
            } else {
                transferFileRequest(line, sSocket);
                input.close();
                sSocket.close();
                numberOfClients--;
            }


        }
    //Request for file transfer done here
    //@param String filename - File being requested
    //@param Socket sSocket - 
    public void transferFileRequest(String fileName, Socket sSocket) throws IOException {

        PrintStream output = 
		new PrintStream(sSocket.getOutputStream(), true);

        InputStream fileInput = null;

        try {
	    //proper usage of program expects the files to be
	    //transfered to be in one folder depth below server
            File file = new File("../Files/" + fileName);
            fileInput = new FileInputStream(file);
            byte[] buffer = new byte[1024];
            int transferAmount = fileInput.read(buffer);
            while (transferAmount > 0) {
		// writing data back into the output stream
                output.write(buffer, 0, transferAmount);
                transferAmount = fileInput.read(buffer);
            }
            output.println("");
            //closing InputStream
            fileInput.close();
            output.println("");
            System.out.println("File successfully transfered");
        } catch (FileNotFoundException e) {
            System.out.println("File can not be located");
        }
    }

    public static void main(String[] args) throws IOException {@
        SuppressWarnings("")
	//creating server
        Server server = new Server();
    }
}
