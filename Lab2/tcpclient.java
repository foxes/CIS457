import java.io.*;
import java.net.*;



class tcpclient{
    public static void main(String args[]) throws Exception{

   	BufferedReader IPFromUser = new BufferedReader(new InputStreamReader(System.in));
   	System.out.println("Enter an IP: ");
	String IP = IPFromUser.readLine();

   	BufferedReader PortFromUser = new BufferedReader(new InputStreamReader(System.in));
   	System.out.println("Enter an port: ");
	int Port = Integer.parseInt(PortFromUser.readLine()); 

        Socket clientSocket = new Socket(IP, Port );
        DataOutputStream outToServer = 
            new DataOutputStream(clientSocket.getOutputStream());
        BufferedReader inFromServer = 
            new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        BufferedReader inFromUser = 
            new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter a message:");
        String message = inFromUser.readLine();
        outToServer.writeBytes(message+ '\n');
        System.out.println('\n' + "Sent your message to the server :) " );

	BufferedReader ServerEcho = new BufferedReader
	   (new InputStreamReader((clientSocket).getInputStream()));
	
	   System.out.println("Your message was: " + ServerEcho.readLine());

    }
 }
