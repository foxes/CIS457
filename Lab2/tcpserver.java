import java.io.*;
import java.net.*;


class tcpserver {
    public static void main (String argv[]) throws Exception {

    BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
    System.out.println("Please enter a port number to connect: ");
    int socketPort = Integer.parseInt(inFromUser.readLine());

        ServerSocket listenSocket = new ServerSocket(socketPort);
        while(true) {
            Socket connectionSocket=listenSocket.accept();
            BufferedReader inFromClient =
                new BufferedReader(
                        new InputStreamReader(
                            connectionSocket.getInputStream()));
            DataOutputStream outToClient =
                new DataOutputStream(
                        connectionSocket.getOutputStream());
            String clientMessage = inFromClient.readLine();
            System.out.println("The client said: " + clientMessage);
            outToClient.writeBytes(clientMessage+'\n');
            connectionSocket.close();
        }
    }
}
